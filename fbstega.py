""" A simple python program that can encrypt messages
    into images and send them to Facebook contacts.
    Can also decrypt messages in images and find facebook
    user IDs.
"""

import re
import sys
import requests
import fbchat
from steganography.steganography import Steganography

def getid(url):
    """ Get the user ID from the given URL """

    idreq = re.compile('"entity_id":"([0-9]+)"')
    page = requests.get(url)
    fid = idreq.findall(page.content)[0]
    return fid

def encode(argv, uname, passw, arg):
    """ Encode a message into an image and send the image through Facebook """

    client = fbchat.Client(uname, passw)
    fid = getid(argv[0])
    encindex = argv.index(arg)
    image = argv[encindex + 1]
    text = argv[encindex + 2]
    outimage = argv[encindex + 3]
    Steganography.encode(image, outimage, text)
    sent = client.sendLocalImage(fid, message=" ", image=outimage)
    if sent:
        print "Message sent succesfully"
    else:
        print "Message was not sent"

def decode(argv, arg):
    """ Decode the message hidden inside the given image """

    decindex = argv.index(arg)
    image = argv[decindex + 1]
    text = Steganography.decode(image)
    print text
    
def helpcom():
    print("Usage: fbstega [URL] [ options ... ] <[original image] [text] [mod image]> <[image]>")
    print("       where options include:")
    print("")
    print("  -e or --encode           Encode text in an image and send it.")
    print("  -d or --decode           Decode text from an image and print it.")
    print("  -i or --getid            Get your friend's ID.")
    print("  -h or --help             Print this message")

def main(argv):
    uname = '<USERID>' #insert your userID here
    passw = '<PASSWORD>' #insert your password here

    if "-e" in argv:
        encode(argv, uname, passw, "-e")
    elif "--encode" in argv:
        encode(argv, uname, passw, "--encode")
    if "-d" in argv:
        decode(argv, "-d")
    elif "--decode" in argv:
        decode(argv, "--decode")
    if "-i" in argv or "--getid" in argv:
        fid = getid(argv[0])
        print fid
    if "-h" in argv or "--help" in argv:
        helpcom()


if __name__ == "__main__":
    main(sys.argv[1:])
